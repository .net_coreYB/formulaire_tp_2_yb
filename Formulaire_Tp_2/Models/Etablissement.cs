﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Formulaire_Tp_2.Models
{
    public class Etablissement
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Nom Etablissement obligatoire")]
        public string nom { get; set; }
        public List<Etudiant> Etudiants { get; set; }
        public int EtudiantId { get; set; }
    }
}
