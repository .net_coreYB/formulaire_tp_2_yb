﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Formulaire_Tp_2.Models
{
    public class Etudiant
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Votre nom est obligatoire")]
        public string nom { get; set; }
        [Required(ErrorMessage = "Votre Prenom est obligatoire")]
        public string prenom { get; set; }
        [Required(ErrorMessage = "Saisie votre classe")]
        public string classe { get; set; }
        [EmailAddress(ErrorMessage = "Saisie un mail Valide Svp !")]
        public string email { get; set; }
        public Etablissement E { get; set; }
    }
}
