﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Formulaire_Tp_2.Models
{
    public class MonContext : DbContext
    {
        public DbSet<Etudiant> Etudiants { get; set; }
        public DbSet<Etablissement> Etablissements { get; set; }
        public MonContext() : base(SqlServerDbContextOptionsExtensions.UseSqlServer(new DbContextOptionsBuilder(), @"Data Source=.\SQLEXPRESS;initial catalog=GestionEtudiants;integrated security=true;").Options)
        {
        }
    }
}
