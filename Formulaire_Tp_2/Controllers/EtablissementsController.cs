﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Formulaire_Tp_2.Models;

namespace Formulaire_Tp_2.Controllers
{
    public class EtablissementsController : Controller
    {
        private readonly MonContext _context;

        public EtablissementsController(MonContext context)
        {
            _context = context;
        }

        // GET: Etablissements
        public async Task<IActionResult> Index()
        {
            return View(await _context.Etablissements.ToListAsync());
        }

        // GET: Etablissements/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var etablissement = await _context.Etablissements
                .FirstOrDefaultAsync(m => m.Id == id);
            if (etablissement == null)
            {
                return NotFound();
            }

            return View(etablissement);
        }

        // GET: Etablissements/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Etablissements/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,nom,EtudiantId")] Etablissement etablissement)
        {
            if (ModelState.IsValid)
            {
                _context.Add(etablissement);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(etablissement);
        }

        // GET: Etablissements/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var etablissement = await _context.Etablissements.FindAsync(id);
            if (etablissement == null)
            {
                return NotFound();
            }
            return View(etablissement);
        }

        // POST: Etablissements/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,nom,EtudiantId")] Etablissement etablissement)
        {
            if (id != etablissement.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(etablissement);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EtablissementExists(etablissement.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(etablissement);
        }

        // GET: Etablissements/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var etablissement = await _context.Etablissements
                .FirstOrDefaultAsync(m => m.Id == id);
            if (etablissement == null)
            {
                return NotFound();
            }

            return View(etablissement);
        }

        // POST: Etablissements/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var etablissement = await _context.Etablissements.FindAsync(id);
            _context.Etablissements.Remove(etablissement);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EtablissementExists(int id)
        {
            return _context.Etablissements.Any(e => e.Id == id);
        }
    }
}
